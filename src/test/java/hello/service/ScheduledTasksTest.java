package hello.service;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import hello.pojo.Person;
import junit.framework.Assert;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/spring/test-application.xml")
public class ScheduledTasksTest {

	@Inject
	private Person person;
	
	
	@Test
	public void testPersonNotNull(){
		Assert.assertTrue("Person should not be null", person!=null);
	}
	
	@Test
	public void testPersonNotZero(){
		Assert.assertTrue("Person age should not be less than 0", person.getAge()>0);
	}
	
	@Test
	public void testPersonAgeIsRight(){
		Assert.assertEquals(10, person.getAge());
	}
}
