package hello.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import hello.pojo.Person;
import hello.pojo.User;

@Component
@EnableAsync
public class ScheduledTasks {

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

	@Inject
	@Named("person")
	private Person person;
	@Autowired
	BackgroundService gitHubLookupService;

	@Scheduled(fixedRate = 5000)
	public void reportCurrentTime(){// throws InterruptedException, ExecutionException, TimeoutException {
		System.out.println("The time is now " + dateFormat.format(new Date()));
		try {
			Future<User> page1 = gitHubLookupService.findUser("PivotalSoftware");
			System.out.println(page1.get(2, TimeUnit.SECONDS));
		} catch (InterruptedException e) {
			System.out.println("InterruptedException | " + e.getMessage());
		} catch (ExecutionException e) {
			System.out.println("ExecutionException | " + e.getMessage());
		} catch (TimeoutException e) {
			System.out.println("TimeoutException | " + e.getMessage());
		} catch (Exception e) {
			System.out.println("Exception | " + e.getMessage());
		}
		System.out.println("Person is " + (person == null ? "not set" : "set"));
		if (person != null)
			System.out.println("Person = " + person.toString());
	}
}
