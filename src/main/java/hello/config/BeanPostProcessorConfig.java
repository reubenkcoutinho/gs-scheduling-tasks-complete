package hello.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import hello.HelloBeanPostProcessor;

@Configuration
public class BeanPostProcessorConfig {

	@Bean
	public HelloBeanPostProcessor getPostProcessor(){
		return new HelloBeanPostProcessor();
	}
}
