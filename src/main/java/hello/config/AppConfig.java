package hello.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import hello.pojo.FunPerson;
import hello.pojo.Person;

@Configuration
@Import(BeanPostProcessorConfig.class)
@PropertySource(value="classpath:/spring/live-config.properties")
public class AppConfig {

//	@Bean(name="person")
//	public Person getPerson(){
//		FunPerson fp = new FunPerson();
//		fp.setAge(10);
//		fp.setName("AppConfig");
//		return fp;
//	}
	
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyConfig(){
		return new PropertySourcesPlaceholderConfigurer();
	}
}
