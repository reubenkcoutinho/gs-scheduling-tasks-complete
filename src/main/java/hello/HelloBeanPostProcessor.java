package hello;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;


public class HelloBeanPostProcessor implements BeanPostProcessor {

	private int order = 0;

	public int getOrder() {
		return order;
	}

	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {

//		if (bean.getClass().isInstance(Person.class))
			System.out.println("HelloBeanPostProcessor-->BeforeInitialization : " + beanName);
		return bean;
	}

	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
//		if (bean.getClass().isInstance(Person.class))
			System.out.println("HelloBeanPostProcessor-->AfterInitialization : " + beanName);
		return bean;
	}

}
