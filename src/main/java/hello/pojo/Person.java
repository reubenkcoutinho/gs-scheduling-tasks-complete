package hello.pojo;


public interface Person {
	public String getName();

	public int getAge();
}
