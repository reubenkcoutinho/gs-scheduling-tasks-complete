package hello.pojo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("person")
public class FunPerson implements Person {

	@Value("${person.name}")
	private String name;
	@Value("${person.age}")
	private int age;
	
	public String toString() {
		return String.format("Name: %s | Age: %d", name, age);
	}
	public FunPerson() {
		System.out.println("FunPerson Construtor called");
	}

	public String getName() {
		System.out.println("FunPerson getName called | name = " + this.name);
		return this.name;
	}

	public void setName(String name) {
		System.out.println("FunPerson setName called | name = " + name);
		this.name=name;
	}

	public int getAge() {
		System.out.println("FunPerson getAge called | age = " + this.age);
		return this.age;
	}

	public void setAge(int age) {
		System.out.println("FunPerson setAge called | age = " + age);
		this.age = age;
	}
	
}
